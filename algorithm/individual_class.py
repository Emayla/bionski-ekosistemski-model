class Individual():
    def __init__(self, solution):
        self.solution = solution
        self.value = None
        self.rank = None
        self.distance = None
        self.dominates = None
        self.dominated_count = None
        self.temp_dominated_count = None

    def __str__(self):
        return str(self.solution)

    def __repr__(self):
        return "Individual({0})".format(self.solution)

    def set_rank(self, rank):
        self.rank = rank

    def set_distance(self):
        if self.distance is None:
            self.distance = 0

    def add_to_distance(self, distance):
        self.distance += distance

    def set_dom(self):
        if self.dominates is None:
            self.dominates = []
        if self.dominated_count is None:
            self.dominated_count = 0

    def add_to_dominates(self, p):
        self.dominates.append(p)

    def add_to_dominated_count(self):
        self.dominated_count += 1

    def subtract_from_dominated_count(self):
        if self.temp_dominated_count is None:
            self.temp_dominated_count = self.dominated_count
        self.dominated_count -= 1

    def restore_dominated_count(self):
        self.dominated_count = self.temp_dominated_count

    def apply_function(self, f):
        self.value = f(self.solution)
        return self.value

    def clear_stats(self):
        self.rank = None
        self.distance = None
        self.dominates = None
        self.dominated_count = None
        self.temp_dominated_count = None