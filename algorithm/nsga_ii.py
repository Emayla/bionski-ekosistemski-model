from individual_class import Individual
import random


def dominates(p, q):
    """
    Returns True, if p dominates q. Otherwise returns false.
    :Individual p: First solution.
    :Individual q: Second solution.
    :return: boolean
    """
    vp = p.value
    vq = q.value
    truly_smaller = False
    for j in range(len(vp)):
        if vp[j] > vq[j]:
            return False  # we are minimizing the objective function
        elif vp[j] < vq[j]:
            truly_smaller = True
    return truly_smaller


def fast_nondominated_sort(population, f):
    """
    Sorts the solutions into nondominated fronts.
    :param population: Array of solutions.
    :param f: Objective function.
    :return: nondom_fronts
    """
    n = len(population)
    nondom_fronts = [[]]
    for i in range(n):
        p = population[i]
        p.set_dom()
        p.apply_function(f)
        for j in range(i+1, n):
            q = population[j]
            q.set_dom()
            q.apply_function(f)
            if dominates(p, q):  # p dominates q
                p.add_to_dominates(q)
                q.add_to_dominated_count()
            elif dominates(q, p):  # q dominates p
                p.add_to_dominated_count()
                q.add_to_dominates(p)
        if p.dominated_count == 0:
            p.set_rank(1)
            nondom_fronts[0].append(p)

    fn = 0  # front number
    while len(nondom_fronts[fn]) > 0:
        temp_front = []
        for p in nondom_fronts[fn]:
            for q in p.dominates:
                q.subtract_from_dominated_count()
                if q.dominated_count == 0:
                    q.set_rank(fn+2)
                    q.restore_dominated_count()
                    temp_front.append(q)
        fn += 1
        nondom_fronts.append(temp_front)

    return nondom_fronts[:len(nondom_fronts)-1]


def crowding_distance_assignment(front):
    """
    Assigns crowding distance to all solutions in a nondominating front.
    :param front:
    :param f: Objective function.
    :return: crowd_distances
    """
    l = len(front)
    if l <= 0:
        return

    for m in range(len(front[0].value)):
        sorted_front = sorted(front, key=lambda x: x.value[m])
        sorted_front[0].set_distance()
        sorted_front[l-1].set_distance()
        sorted_front[0].add_to_distance(float("inf"))
        sorted_front[l-1].add_to_distance(float("inf"))
        mmin = sorted_front[0].value[m]
        mmax = sorted_front[l-1].value[m]

        for j in range(1, l-1):
            p = sorted_front[j]
            p.set_distance()
            vprev = sorted_front[j-1].value[m]
            vnext = sorted_front[j+1].value[m]
            p.add_to_distance((vnext-vprev) / (mmax-mmin))
    return front


def crowded_comparison_operator(p, q):
    """
    Returns True, if p '<' q.
    :return: boolean
    """
    if p.rank < q.rank:  # prank < qrank
        return True
    elif p.rank == q.rank:
        if p.distance > q.distance:
            return True
        elif p.distance == q.distance:
            return "equal"
    return False


def sort_crowded_comparison(front):
    """
    Sorts nondominated solutions based on the crowded comparison operator.
    :param front: Solutions.
    :return: Sorted front.
    """
    def sort_key(x):
        return (x.rank, -x.distance)

    front = sorted(front, key=sort_key)
    return front


def tournament_selection(population, k):
    """
    Selects k random individuals from population and returns the best based on fitness and domination rank.
    :param population:
    :param k:
    :return:
    """
    winner = None
    for j in range(k):
        # p = pop[random.randint(0, n-1)]
        p = random.choice(population)
        # print("Random choice number {0}: {1}".format(j+1, p))
        if winner is None:  # or (p.rank < winner.rank):
            winner = p
        elif crowded_comparison_operator(p, winner) is True:
            winner = p
        elif crowded_comparison_operator(p, winner) == 'enako':
            p_dom_rank = len(p.dominates) - p.dominated_count
            winner_dom_rank = len(winner.dominates) - winner.dominated_count
            if p_dom_rank > winner_dom_rank:
                winner = p
            elif p_dom_rank == winner_dom_rank:
                winner = random.choice([p, winner])
    return winner


def repair_solution(sol, index, constraints):
    """
    Repairs solution sol in order to make it feasible again.
    :param sol:
    :param index:
    :param constraints:
    :return:
    """
    def repair_one(ssol, aa):
        x = random.choice(aa)
        aa.remove(x)
        if sum(ssol) < E:
            ssol[x] += min(E - sum(ssol), constraints[x][1] - ssol[x])
        elif sum(ssol) > E:
            ssol[x] -= min(sum(ssol) - E, ssol[x] - constraints[x][0])
        return ssol, aa

    l = len(constraints)
    E = constraints[l-1]
    a = list(range(index, l-1))
    while sum(sol) != E and a:
        sol, a = repair_one(sol, a)
    if sum(sol) != E:
        a = list(range(index))
        while sum(sol) != E and a:
            sol, a = repair_one(sol, a)

    return sol


def crossover(parent1, parent2, crossover_rate, constraints):
    def one_point_crossover(psol1, psol2, length):
        j = random.randint(1, length - 1)
        # print("Index:", j)
        csol1 = []
        csol2 = []
        for i in range(j):
            csol1.append(psol1[i])
            csol2.append(psol2[i])
        for i in range(j, length):
            csol1.append(psol2[i])
            csol2.append(psol1[i])
        return csol1, csol2, j

    def two_point_crossover(psol1, psol2, length):
        j = random.randint(length/2 - 4, length/2 + 4)
        k = random.randint(j+1, j+5)
        # print("Index:", j)
        csol1 = []
        csol2 = []
        for i in range(j-1):
            csol1.append(psol1[i])
            csol2.append(psol2[i])
        for i in range(j-1, k):
            csol1.append(psol2[i])
            csol2.append(psol1[i])
        for i in range(k, length):
            csol1.append(psol1[i])
            csol2.append(psol2[i])
        return csol1, csol2, j

    if random.random() > crossover_rate:
        return parent1, parent2
    l = len(constraints)
    E = constraints[l-1]
    ps1 = parent1.solution
    ps2 = parent2.solution
    if l > 20:
        cs1, cs2, ind = two_point_crossover(ps1, ps2, l-1)
    else:
        cs1, cs2, ind = one_point_crossover(ps1, ps2, l-1)
    if sum(cs1) != E:
        cs1 = repair_solution(cs1, ind, constraints)
    if sum(cs2) != E:
        cs2 = repair_solution(cs2, ind, constraints)
    return Individual(tuple(cs1)), Individual(tuple(cs2))


def mutation(p, mutation_rate, constraints):
    ps = list(p.solution)
    l = len(ps)
    for i in range(l):
        if random.random() <= mutation_rate:
            ps[i] = random.randint(*constraints[i])
    # print(ps)
    ps = repair_solution(ps, 0, constraints)
    return Individual(tuple(ps))


def clear_population_stats(population):
    """
    Clears the rank, distance, dom stats for the whole population.
    :param population:
    :return:
    """
    for p in population:
        p.clear_stats()


def create_random_population(n, constraints):
    population = []
    while n > 0:
        sol = []
        for i in range(len(constraints)-1):
            sol.append(random.randint(*constraints[i]))
        sol = repair_solution(sol, 0, constraints)
        population.append(Individual(tuple(sol)))
        n -= 1
    return population


def make_new_population(population, constraints):
    n = len(population)
    new_population = []
    while n > 0:
        p1 = tournament_selection(population, 2)
        p2 = tournament_selection(population, 2)
        c1, c2 = crossover(p1, p2, 4/5, constraints)
        c1 = mutation(c1, 1/100, constraints)
        c2 = mutation(c2, 1/100, constraints)
        if n == 1:
            new_population.append(random.choice([c1, c2]))
            n -= 1
        else:
            new_population.append(c1)
            new_population.append(c2)
            n -= 2
    return new_population


def main_loop():
    func = lambda x: (x[0]**3 + 4*x[0]**2 + 1, 2*x[1]**3 - x[1]**2 - 17*x[1], 50*x[2] + 10)
    n = 100
    cons = [(0, 9), (6, 10), (0, 7), 15]
    gen_number = 100

    pop = create_random_population(n, cons)
    print("Začetna populacija:", pop)
    print("------------------------------------------")
    pop = fast_nondominated_sort(pop, func)
    # print("Sortirana populacija:", pop)
    for front in pop:
        crowding_distance_assignment(front)
    # print("Populacija po assignmentu distancea:", pop)
    pop = [sol for front in pop for sol in front]
    new_pop = make_new_population(pop, cons)

    gen = 1
    while gen <= gen_number:
        # print("%%%%%%%%%%%%%%%%%")
        # print("Stara populacija:", pop)
        # print("Nova populacija:", new_pop)
        comb_pop = pop + new_pop
        clear_population_stats(comb_pop)
        comb_pop = fast_nondominated_sort(comb_pop, func)
        next_pop = []
        fn = 0
        while len(next_pop) + len(comb_pop[fn]) <= n:
            crowding_distance_assignment(comb_pop[fn])
            next_pop += comb_pop[fn]
            fn += 1
        crowding_distance_assignment(comb_pop[fn])
        sorted_front = sort_crowded_comparison(comb_pop[fn])
        next_pop += sorted_front[:n-len(next_pop)+1]

        pop = next_pop
        new_pop = make_new_population(pop, cons)
        gen += 1
    print("-------------------------")
    print("Končna populacija:", pop)
    return pop


# def objf(x):
#     results = [(3, 4), (1, 5), (2, 5), (6, 7), (2, 3)]
#     return results[x]
#
# pop = [Individual(i) for i in range(5)]
# sorted_pop = fast_nondominated_sort(pop, objf)
# print(sorted_pop)
# for count, f in enumerate(sorted_pop):
#     print("Fronta", count)
#     for ind in f:
#         print(ind.solution, ind.rank, ind.dominates, ind.dominated_count)
#
# distanced_pop = crowding_distance_assignment(pop)
# for ind in distanced_pop:
#     print(ind.solution, ind.distance)
# print(sort_crowded_comparison(pop))
# print(tournament_selection(pop, 2))
#
# par1 = Individual((1, 7, 7, 6, 14, 12, 3, 5, 12, 1, 1, 4))
# par2 = Individual((13, 3, 7, 1, 11, 7, 6, 6, 11, 3, 2, 3))
# const = ((0, 15), (2, 9), (1, 14), (0, 13), (6, 21), (3, 16), (1, 8), (4, 7), (8, 14), (0, 5), (1, 10), (2, 8), 73)
# c1, c2 = crossover(par1, par2, 1, const)
# print("Child 1", c1)
# print("Child 2", c2)
# print("Mutated child 1", mutation(c1, 0.1, const))
main_loop()